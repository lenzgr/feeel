import 'dart:ui';

class LocaleHelper {
  static final supportedLocales = [
    const Locale('en'),
    const Locale('ar'),
    const Locale('cs'),
    const Locale('de'),
    const Locale('el'),
    const Locale('es'),
    const Locale('fr'),
    const Locale('hr'),
    const Locale('id'),
    const Locale('it'),
    const Locale('nl'),
    const Locale('pt'),
    const Locale('ru'),
    const Locale('tr'),
    const Locale('zh'),
  ];
}
